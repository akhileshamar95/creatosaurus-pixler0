import React, {Component} from 'react';
import Home from './components/home';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Login from './components/Login'
import Signup from './components/Signup';
import Loginnavbar from './components/loginNavbar';
import Profile from './components/profile'
import Stats from './components/stats';
import Statsupdate from './components/statsupdate';
import Statschangepassword from './components/statschangepassword';
import search from './components/search';
import hoverdown from './components/hoveringon';



class App extends Component{
  render(){
    return(
      
      <Router>
   
      <Switch>
        <Route exact path="/" component={Home}/>
        <Route path="/Login" component={Login}/>
        <Route path="/Signup" component={Signup}/>
        <Route path="/Loginnavbar" component={Loginnavbar}/>
        <Route path="/Profile" component={Profile}/>
        <Route path="/Stats" component={Stats}/>
        <Route path="/Statsupdate" component={Statsupdate}/>
        <Route path="/changepassword" component={Statschangepassword}/>
        <Route path="/search" component={search} />
        <Route path="/hoverdownload" component={hoverdown} />
        
      </Switch>
   
  </Router>
      
    
    )
  }
}


export default App;
