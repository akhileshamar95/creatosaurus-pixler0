import React, { Component } from 'react';

import '../components/AllpageCss/search.css';

import pixler from '../assets/image/pixler.png';

import spring from '../../src/assets/image/spring.png';
import tress1 from '../../src/assets/image/tress1.png';
import seabank from '../../src/assets/image/seabank.png';
import tress from '../../src/assets/image/trees12.png';
import flowers from '../../src/assets/image/flowers.png';
import sunsetbird from '../../src/assets/image/sunsetbird.png';

import facebook from '../assets/image/facebook.png';
import instagram from '../../src/assets/image/instagram.png';
import twiter from '../../src/assets/image/twiter.png';
import linkedIn from '../../src/assets/image/linkedIn.png';
import heart from '../../src/assets/image/heart.png';



class search extends Component{
render(){
  return(

    <div className="main-container">
      {/** navbar start */}    
      <div className="topnav">
         <a className="active" href="#pixler" ><img className="img-class" src={pixler} alt="pixler" /></a>
         <form action="#" class="center12">
            <div className="search12">
             <input  type="text" name="search" placeholder="search for free photos" class="round" />
             <input type="submit" class="corner" value="" />
           </div>
       </form>
         
         
         
         
         <div className="topnav-right1">
            <a className="rectangle" href="#Submit a Photo"><span className="submitbuttoncolor">Submit a Photo</span></a>
            <a className="signup" href="/Signup">Sing UP</a>
           <a className="login" href="/Login">Log In</a>
           
        </div>
      </div>
      {/** navbar close */}
       {/** nature start */}
       <div class="topnav1">
         <a class="active" href="#home">Nature</a>
        
          <div class="topnav-right">
            <div className="natureviews">Sort by
               <button className="naturenew">new</button>
             </div>
          </div>
      </div>

       {/** bodycontent start */}
      <div class="container">
      <div className="image-container"><img className="img-class" src={spring} alt="component1" /></div>
      <div className="image-container"><img className="img-class" src={tress1} alt="component2" /></div>
      <div className="image-container"><img className="img-class" src={seabank} alt="component3" /></div>
      <div className="image-container"><img className="img-class" src={tress} alt="component4" /></div>
      <div className="image-container"><img className="img-class" src={flowers} alt="component5" /></div>
      <div className="image-container" ><img className="img-class" src={sunsetbird} alt="component6" /></div>
      </div>
      {/*image container close*/}




      <div className="loadmore-container" >
      <div class="loadmore11">
      <span className="mouserover12">
        <button class="loadmore-bottom1"><a class="loadmore-bottom1" href="#loandmore">Load More</a></button></span>
      </div>
      
     </div>

   {/* footer */}
   <div class="footer1">
   <div class="footer-col-container">
    <div class="footer-col" >
     <div class="footer-grid-item">
     <div className="active1">𝚙 𝚒 𝕏 𝚕 𝚎 𝚛</div>
     <pre class="margin">
       freely available image by<br/> 
            our talented communitry.</pre> </div>
    <pre class="margin">
     <div><span>&nbsp; <a href="#facebook"><img  src={facebook} alt="Facebook" /></a></span>
     <span>&nbsp; <a href="#instagram"><img  src={instagram} alt="instagram" /></a></span>
     <span>&nbsp; <a href="#twiter"><img  src={twiter} alt="twiter"/></a></span>
     <span>&nbsp; <a href="#linkedIn"><img  src={linkedIn}  alt="linkedIn"/></a></span>
     </div>
    </pre>
 </div>
   {/* <!--About--> */}
<div class="footer-col" >
 <h2>About</h2>
 <pre class="footer-grid-item">About us </pre> 
 <pre class="footer-grid-item">Contact us </pre> 
 <pre class="footer-grid-item">Report a Bug </pre> 
 <pre class="footer-grid-item">Feedback </pre> 
</div>

{/* <!--Community--> */}
<div class="footer-col" >
 <h2>community</h2>
 <pre class="footer-grid-item">Volunteer with us </pre> 
 <pre class="footer-grid-item">Represent us </pre> 
 <pre class="footer-grid-item">Donate </pre> 
 <pre class="footer-grid-item">Host a Meetup </pre> 
 
</div>
{/* <!--Contant--> */}
<div class="footer-col">
 <h2>Content</h2>
 <pre class="footer-grid-item">Submit a photos </pre> 
 <pre class="footer-grid-item">Are you a brand </pre> 
</div>
{/* <!--Terms and condition--> */}
<div class="footer-col" >
 <h2>Terms and Conditions</h2>
 <pre class="footer-grid-item">Privacy Policy </pre> 
 <pre class="footer-grid-item">Copyright Notice </pre> 
 <pre class="footer-grid-item">Terms of use </pre> 
 <pre class="footer-grid-item">Pixler License</pre> 
 <pre class="footer-grid-item">Submitting Guidelines </pre>
</div>
</div>

<hr class="hrclass" />
 <div class="margin1" >
     <samp class="footer-grid-item" > Parts of <span class="margin2">Creatosaurus</span> family of apps</samp> 

     <samp class="margin3" >Made with<span>&nbsp;</span><img  src={heart} alt="heart" /> <span>&nbsp;</span> in India</samp>  
   
   </div>
  </div>
       
    

  

     </div>
    

   
    
  );
}
 
  
  }

export default search;
