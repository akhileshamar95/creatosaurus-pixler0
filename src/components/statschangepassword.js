import React, { Component } from 'react';

import '../components/AllpageCss/statschangepassword.css';
import personroundpic from '../assets/image/personroundPic.png';

import pixler from '../assets/image/pixler.png';

import facebook from '../assets/image/facebook.png';
import instagram from '../../src/assets/image/instagram.png';
import twiter from '../../src/assets/image/twiter.png';
import linkedIn from '../../src/assets/image/linkedIn.png';
import heart from '../../src/assets/image/heart.png';
class statschangepassword extends Component{
render(){
  return(

    <div className="main-container">
      {/** navbar start */}    
      <div className="topnav">
         <a className="active" href="#pixler" ><img className="img-class" src={pixler} alt="pixler" /></a>
          <div className="topnav-right1">
            <a className="rectangle" href="#Submit a Photo"><span className="submitbuttoncolor">Submit a Photo</span></a>
            <span className="person">
              <div className="personround"><img  src={personroundpic} alt="personroundpic" />
             </div>
 
            </span>
            <span className="personakshat">Akshat saxena</span>
        </div>
      </div>
      {/** navbar close */}

      {/** bodycontent start */}
        <div className="divcontainer3">
        <div class="divrow3">
          <div class="divcolumn31" >
          <a className="basicinfo" href="statsupdate">Basic Information </a>
          <a className="basicinfo" href="changepassword"><p>change Password </p></a>
          <a className="basicinfo" href="connectedaccount"><p>Connected Accounted </p></a>
          
         </div>
       
           <div class="divcolumn331" >
           <div className="formcontainer21">
           <form action="update">
             <div class="row11">
               <div class="col-25">
                 <label >Current Password</label><br />
              
                  <div class="col-75">
                   <input className="input21" type="password" id="fname" name="password" placeholder="Current Password" />
                  </div>
                </div>
             </div>

             <div class="row11">
               <div class="col-25">
                 <label >New Password</label><br />
                  <div class="col-75">
                   <input className="input21" type="password" id="lname" name="newpassword" placeholder="New Password" />
                  </div>
                </div>
             </div>
             <div class="row11">
               <div class="col-25">
                 <label >Repeat Password</label><br />
                  <div class="col-75">
                   <input className="input21" type="password" id="email" name="repeatpassword" placeholder="Repeat Password" />
                  </div>
                </div>
             </div>

           <div class="row">
           <input className="updatesubmit" type="submit" value="Update" />
         </div>

           </form>

           </div>
         
           </div>

       </div>
     

        </div>


      
      {/* footer start */}
     
      <div class="footer1">
      <div class="footer-col-container">
      <div class="footer-col" >
        <div class="footer-grid-item">
           <div className="active1">𝚙 𝚒 𝕏 𝚕 𝚎 𝚛</div>
           <pre class="margin">
               freely available image by<br/> 
                      our talented communitry.</pre>
       </div>
       <pre class="margin">
        <div><span>&nbsp; <a href="#facebook"><img  src={facebook} alt="Facebook" /></a></span>
         <span>&nbsp; <a href="#instagram"><img  src={instagram} alt="instagram" /></a></span>
         <span>&nbsp; <a href="#twiter"><img  src={twiter} alt="twiter"/></a></span>
         <span>&nbsp; <a href="#linkedIn"><img  src={linkedIn}  alt="linkedIn"/></a></span>
       </div>
      </pre>
   </div>
      {/* <!--About--> */}
  <div class="footer-col" >
    <h2>About</h2>
    <pre class="footer-grid-item">About us </pre> 
    <pre class="footer-grid-item">Contact us </pre> 
    <pre class="footer-grid-item">Report a Bug </pre> 
    <pre class="footer-grid-item">Feedback </pre> 
  </div>

  {/* <!--Community--> */}
  <div class="footer-col" >
    <h2>community</h2>
    <pre class="footer-grid-item">Volunteer with us </pre> 
    <pre class="footer-grid-item">Represent us </pre> 
    <pre class="footer-grid-item">Donate </pre> 
    <pre class="footer-grid-item">Host a Meetup </pre> 
    
  </div>
  {/* <!--Contant--> */}
   <div class="footer-col">
 
    <h2>Content</h2>
    <pre class="footer-grid-item">Submit a photos </pre> 
    <pre class="footer-grid-item">Are you a brand </pre> 
    
  </div>
  {/* <!--Terms and condition--> */}
   <div class="footer-col" >
    <h2>Terms and Conditions</h2>
    <pre class="footer-grid-item">Privacy Policy </pre> 
    <pre class="footer-grid-item">Copyright Notice </pre> 
    <pre class="footer-grid-item">Terms of use </pre> 
    <pre class="footer-grid-item">Pixler License</pre> 
    <pre class="footer-grid-item">Submitting Guidelines </pre>
  </div>
</div>

      <hr class="hrclass" />
       <div class="margin1" >
         <samp class="footer-grid-item" > Parts of <span class="margin2">Creatosaurus</span> family of apps</samp> 

         <samp class="margin3" >Made with<span>&nbsp;</span><img  src={heart} alt="heart" /> <span>&nbsp;</span> in India</samp>  
      
       </div>

     </div>
     </div>

    
     

    
    
    
  );
}
 
  
  }

export default statschangepassword;
