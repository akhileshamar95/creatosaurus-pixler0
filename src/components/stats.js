import React, { Component } from 'react';

import '../components/AllpageCss/stats.css';
import personroundpic from '../assets/image/personroundPic.png';
import profieperson from '../../src/assets/image/profieperson.png';
import pixler from '../assets/image/pixler.png';

import facebook from '../assets/image/facebook.png';
import instagram from '../../src/assets/image/instagram.png';
import twiter from '../../src/assets/image/twiter.png';
import linkedIn from '../../src/assets/image/linkedIn.png';
import heart from '../../src/assets/image/heart.png';
class stats extends Component{
render(){
  return(

    <div className="main-container">
      {/** navbar start */}    
      <div className="topnav">
         <a className="active" href="#pixler" ><img className="img-class" src={pixler} alt="pixler" /></a>
          <div className="topnav-right1">
            <a className="rectangle" href="#Submit a Photo"><span className="submitbuttoncolor">Submit a Photo</span></a>
            <span className="person">
              <div className="personround"><img  src={personroundpic} alt="personroundpic" />
             </div>
 
            </span>
            <span className="personakshat">Akshat saxena</span>
        </div>
      </div>
      {/** navbar close */}

      {/** bodycontent start */}

      <div className="bodycontent1">
       <div className="personround1"><img  src={profieperson} alt="profieperson" /> 
        <div className="textcenter1">Akshat saxsena</div><br></br>
        <div className="textcolor21">@Sheldonand</div>
        </div>
      <div >
      <div className="paragraph1">
         Hi, I'm a french design lover in shoreditch. <br />
        i'm currently designer in my awesomw team at impero Landon, after three years <br />
                                   spent in paris as brand designer
        </div>
        <a href="#Gotham Rounded"><pre className="m0r-im-over-getintouch1">
        <span className="GetIntouchButton1">Get in touch</span></pre> </a>
        </div>
        </div><br></br>
        
       
      {/** Insights start */}
      <div className="insigntHeight">
      <div className="center21">Insights</div>
           <div class="grid-container21">
             <div class="grid-item21">
                 <p className="view21">Views</p>
                 <p className="view100">100</p>
             </div>
             <div class="grid-item21">
                 <p className="view21">Download</p>
                 <p className="view100">10</p>
            </div>
         </div>
      </div>

      {/* footer start */}
      <div className="footerheight1">
      <div class="footer1">
      <div class="footer-col-container">
      <div class="footer-col" >
        <div class="footer-grid-item">
           <div className="active1">𝚙 𝚒 𝕏 𝚕 𝚎 𝚛</div>
           <pre class="margin">
               freely available image by<br/> 
                      our talented communitry.</pre>
       </div>
       <pre class="margin">
        <div><span>&nbsp; <a href="#facebook"><img  src={facebook} alt="Facebook" /></a></span>
         <span>&nbsp; <a href="#instagram"><img  src={instagram} alt="instagram" /></a></span>
         <span>&nbsp; <a href="#twiter"><img  src={twiter} alt="twiter"/></a></span>
         <span>&nbsp; <a href="#linkedIn"><img  src={linkedIn}  alt="linkedIn"/></a></span>
       </div>
      </pre>
   </div>
      {/* <!--About--> */}
  <div class="footer-col" >
    <h2>About</h2>
    <pre class="footer-grid-item">About us </pre> 
    <pre class="footer-grid-item">Contact us </pre> 
    <pre class="footer-grid-item">Report a Bug </pre> 
    <pre class="footer-grid-item">Feedback </pre> 
  </div>

  {/* <!--Community--> */}
  <div class="footer-col" >
    <h2>community</h2>
    <pre class="footer-grid-item">Volunteer with us </pre> 
    <pre class="footer-grid-item">Represent us </pre> 
    <pre class="footer-grid-item">Donate </pre> 
    <pre class="footer-grid-item">Host a Meetup </pre> 
    
  </div>
  {/* <!--Contant--> */}
   <div class="footer-col">
 
    <h2>Content</h2>
    <pre class="footer-grid-item">Submit a photos </pre> 
    <pre class="footer-grid-item">Are you a brand </pre> 
    
  </div>
  {/* <!--Terms and condition--> */}
   <div class="footer-col" >
    <h2>Terms and Conditions</h2>
    <pre class="footer-grid-item">Privacy Policy </pre> 
    <pre class="footer-grid-item">Copyright Notice </pre> 
    <pre class="footer-grid-item">Terms of use </pre> 
    <pre class="footer-grid-item">Pixler License</pre> 
    <pre class="footer-grid-item">Submitting Guidelines </pre>
  </div>
</div>

      <hr class="hrclass" />
       <div class="margin1" >
         <samp class="footer-grid-item" > Parts of <span class="margin2">Creatosaurus</span> family of apps</samp> 

         <samp class="margin3" >Made with<span>&nbsp;</span><img  src={heart} alt="heart" /> <span>&nbsp;</span> in India</samp>  
      
       </div>

     </div>
     </div>
     {/* footer close */}

     </div>
    
    
  );
}
 
  
  }

export default stats;
