import React, { Component } from 'react';

import '../components/AllpageCss/loginNavbar.css';
import personroundpic from '../assets/image/personroundPic.png';
import component1 from '../../src/assets/image/component1.png';
import component2 from '../../src/assets/image/component2.png';
import component3 from '../../src/assets/image/component3.png';
import component4 from '../../src/assets/image/component4.png';
import component5 from '../../src/assets/image/component5.png';
import component6 from '../../src/assets/image/component6.png';
import component7 from '../../src/assets/image/component7.png';
import component8 from '../../src/assets/image/component8.png';
import component9 from '../../src/assets/image/component9.png';
import facebook from '../assets/image/facebook.png';
import instagram from '../../src/assets/image/instagram.png';
import twiter from '../../src/assets/image/twiter.png';
import linkedIn from '../../src/assets/image/linkedIn.png';
import heart from '../../src/assets/image/heart.png';
import person from '../assets/image/person.png';
import download from '../assets/image/downloadpic.png'
import pixler from '../assets/image/pixler.png';


class loginNavbar extends Component{
render(){
  return(

    <div className="main-container">

       <div className="topnav">
        <a className="active" href="#pixler" ><img className="img-class" src={pixler} alt="pixler" /></a>
         <div className="topnav-right1">
          <a className="rectangle" href="#Submit a Photo"><span className="submitbuttoncolor">Submit a Photo</span></a>
           <span className="person">
            <div className="personround"><img  src={personroundpic} alt="personroundpic" /></div></span>
          {/**dropdown containts start */}
          <span class="dropdown1">
           <span class="dropbtn1">Akshat saxena
             <span class="dropdown-content1">
             <span className="positionabsolute">
              <div className="forntsizedrop"><a href="profile"><span className="forntsizedrop" >profile </span></a></div> 
              <div className="forntsizedrop"><a href="#Insights"><span className="forntsizedrop"  >Insights </span></a></div> 
              <div className="forntsizedrop"><a href="#Account settings"><span className="forntsizedrop"  >AccountSettings </span></a></div> 
              <div className="forntsizedrop"><a href="#Logout"><span className="forntsizedrop"  >  Logout </span></a></div> 
            </span>
           </span>
          </span>
          </span>
           {/**dropdown containts close */}
        </div>
       </div>

      {/*image container start*/}
      <div class="container">
      <div className="image-container"><img className="img-class" src={component1} alt="component1" />
        <div className="circle-round"><a href="#Gotham Rounded"><img className="img-class" src={person} alt="person" /></a>
            {/* over mouse */}
         <div className="overlay-container">
           <div className="overlay">
            <div className="circle-round1"><a href="#Gotham Rounded"><img className="img-class" src={person} alt="person" /></a>
             <a href="#Gotham Rounded"><pre className="m0r-im-over-text">Gotham Rounded</pre> </a>
             <a href="#Gotham Rounded"><pre className="m0r-im-over-text1">@GothamRounded</pre> </a>
            </div>
            <a href="#Gotham Rounded"><pre className="m0r-im-over-getintouch">
            <span className="GetIntouchButton">Get in touch</span></pre> </a>
          
           </div>
          </div>
          <a href="#Gotham Rounded"><pre className="image-over-text ">Gotham Rounded</pre> </a>
       

          <div className="downloan-image-icon"><a href="#download"><img className="img-class" src={download} alt="download" /></a></div>

        </div>
        
      </div>

      <div className="image-container"><img className="img-class" src={component2} alt="component4" />
     
      </div>
      <div className="image-container"><img className="img-class" src={component3} alt="component7" /> </div>
      <div className="image-container"><img className="img-class" src={component4} alt="component2" />
      <div className="circle-round"><a href="#Gotham Rounded"><img className="img-class" src={person} alt="person" /></a>
      {/* over mouse */}
     <div className="overlay-container">
      <div className="overlay">
       <div className="circle-round1"><a href="#Gotham Rounded"><img className="img-class" src={person} alt="person" /></a>
         <a href="#Gotham Rounded"><pre className="m0r-im-over-text">Gotham Rounded</pre> </a>
         <a href="#Gotham Rounded"><pre className="m0r-im-over-text1">@GothamRounded</pre> </a>
       </div>
         <a href="#Gotham Rounded"><pre className="m0r-im-over-getintouch">
         <span className="GetIntouchButton">Get in touch</span></pre> </a>
    
      </div>
     </div>
     <a href="#Gotham Rounded"><pre className="image-over-text ">Gotham Rounded</pre> </a>
 

     <div className="downloan-image-icon"><a href="#download"><img className="img-class" src={download} alt="download" /></a></div>

     </div>
      </div>
      <div className="image-container"><img className="img-class" src={component5} alt="component5" /></div>
      <div className="image-container"><img className="img-class" src={component8} alt="component8" /></div>
      <div className="image-container"><img className="img-class" src={component7} alt="component3" /></div>
      <div className="image-container"><img className="img-class" src={component6} alt="component6" /></div>
      <div className="image-container" ><img className="img-class" src={component9} alt="component9" /></div>
      
      </div>
      {/*image container close*/}

      {/*LoadMore option start*/}

      <div className="loadmore-container" >
        <div class="loadmore11">
        <span className="mouserover12"> <button class="loadmore-bottom1"><a class="loadmore-bottom1" href="#loandmore">Load More</a></button></span>
       </div>
      </div>
     {/*LoadMore option close*/}

      {/* footer start */}
      <div class="footer1">
      <div class="footer-col-container">
      <div class="footer-col" >
        <div class="footer-grid-item">
           <div className="active1">𝚙 𝚒 𝕏 𝚕 𝚎 𝚛</div>
           <pre class="margin">
               freely available image by<br/> 
                      our talented communitry.</pre>
       </div>
       <pre class="margin">
        <div><span>&nbsp; <a href="#facebook"><img  src={facebook} alt="Facebook" /></a></span>
         <span>&nbsp; <a href="#instagram"><img  src={instagram} alt="instagram" /></a></span>
         <span>&nbsp; <a href="#twiter"><img  src={twiter} alt="twiter"/></a></span>
         <span>&nbsp; <a href="#linkedIn"><img  src={linkedIn}  alt="linkedIn"/></a></span>
       </div>
      </pre>
   </div>
      {/* <!--About--> */}
  <div class="footer-col" >
    <h2>About</h2>
    <pre class="footer-grid-item">About us </pre> 
    <pre class="footer-grid-item">Contact us </pre> 
    <pre class="footer-grid-item">Report a Bug </pre> 
    <pre class="footer-grid-item">Feedback </pre> 
  </div>

  {/* <!--Community--> */}
  <div class="footer-col" >
    <h2>community</h2>
    <pre class="footer-grid-item">Volunteer with us </pre> 
    <pre class="footer-grid-item">Represent us </pre> 
    <pre class="footer-grid-item">Donate </pre> 
    <pre class="footer-grid-item">Host a Meetup </pre> 
    
  </div>
  {/* <!--Contant--> */}
   <div class="footer-col">
 
    <h2>Content</h2>
    <pre class="footer-grid-item">Submit a photos </pre> 
    <pre class="footer-grid-item">Are you a brand </pre> 
    
  </div>
  {/* <!--Terms and condition--> */}
   <div class="footer-col" >
    <h2>Terms and Conditions</h2>
    <pre class="footer-grid-item">Privacy Policy </pre> 
    <pre class="footer-grid-item">Copyright Notice </pre> 
    <pre class="footer-grid-item">Terms of use </pre> 
    <pre class="footer-grid-item">Pixler License</pre> 
    <pre class="footer-grid-item">Submitting Guidelines </pre>
  </div>
</div>

      <hr class="hrclass" />
       <div class="margin1" >
         <samp class="footer-grid-item" > Parts of <span class="margin2">Creatosaurus</span> family of apps</samp> 

         <samp class="margin3" >Made with<span>&nbsp;</span><img  src={heart} alt="heart" /> <span>&nbsp;</span> in India</samp>  
      
       </div>

     </div>
     {/* footer close */}




     </div>
    
    
  );
}
 
  
  }

export default loginNavbar;
